#!/bin/sh

cd ~/octoprint-type-a

apt-get install g++ curl pkg-config libv4l-dev libjpeg-dev libjpeg8-dev build-essential libssl-dev vim cmake imagemagick subversion -y

wget https://github.com/shrkey/mjpg-streamer/raw/master/mjpg-streamer.tar.gz

tar -xvf ./mjpg-streamer.tar.gz

cd mjpg-streamer

make clean all

make install

cd ~/octoprint-type-a

rm mjpg-streamer.tar.gz

git clone git://git.videolan.org/x264.git
cd x264
./configure --enable-shared --prefix=/usr
make
make install

cd ~/octoprint-type-a

git clone git://git.videolan.org/ffmpeg.git
cd ffmpeg
./configure --enable-shared --enable-libx264 --enable-gpl
git remote set-url origin git://source.ffmpeg.org/ffmpeg
make
make install

chown /dev/video0 ubuntu

cd ~/octoprint-type-a

{ echo 'include /usr/local/lib'; } >> /etc/ld.so.conf

ldconfig

mjpg-streamer -v
x264 -v
ffmpeg -v


