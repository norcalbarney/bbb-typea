cd ~/octoprint-type-a

apt-get install python-numpy -y

git clone https://github.com/daid/Cura.git 

cd Cura 

git clone https://github.com/Ultimaker/CuraEngine.git 

cd CuraEngine 

make

cp build/* -r ./
 
cd ~/octoprint-type-a/Cura

{ echo '#!/bin/sh'; echo 'PYTHONPATH=. python2 -m Cura.cura $@'; } >> cura.sh 

chmod +x cura.sh
