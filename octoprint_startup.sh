#!/bin/sh

OCTOPRINT_HOME=/home/ubuntu/octoprint-type-a

MJPEG_STREAMER_HOME=/home/ubuntu/octoprint-type-a/mjpg-streamer

# start mjpeg streamer

$MJPEG_STREAMER_HOME/mjpg_streamer -i "$MJPEG_STREAMER_HOME/input_uvc.so -r 640x480 -f 30" -o "$MJPEG_STREAMER_HOME/output_http.so" &

# start the webui

./run
